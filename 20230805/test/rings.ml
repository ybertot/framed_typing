open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let plus_a_b = "add @ a @ b",
                 left "((Radd (R_of_Z (Z.of_N a))) b): R"
  and plus_a_c = "add @ a @ c",
                 left "((RquotIadd (R_modI (R_of_Z (Z.of_N a)))) c): RquotI"
  and plus_c_1 = "add @ c @ 1",
                 left "((RquotIadd c) (R_modI (R_of_Z (Z.of_N 1%N)))): RquotI"
  and lambda_x_plus_1 = "fun x: N |-> add @ x @ -1",
                        left "(fun x => ((Z.add (Z.of_N x)) (-1)%Z)): (N -> Z)"
  and plus_a_annot = "(add @ a): R -> R",
                     left "(Radd (R_of_Z (Z.of_N a))): (R -> R)"
  and plus_c = "add @ c",
               left "(RquotIadd c): (RquotI -> RquotI)"
  and plus_a = "add @ a",
               left "(N.add a): (N -> N)"
  and plus_a_b_c = "add @ (add @ a @ b) @ c",
                   left "((RquotIadd (R_modI ((Radd (R_of_Z (Z.of_N a))) b))) c): RquotI"
  and plus_two_vars = "fun x |-> (fun y |-> add @ x @ y)",
                      left "(fun x => (fun y => ((N.add x) y))): (N -> (N -> N))"
  and plus_two_vars_annot = "fun x: R |-> (fun y |-> add @ x @ y)",
                            left "(fun x => (fun y => ((Radd x) y))): (R -> (R -> R))"
  in
  [plus_a_b; plus_a_c; plus_c_1; lambda_x_plus_1; plus_a_annot;
   plus_c; plus_a; plus_a_b_c; plus_two_vars; plus_two_vars_annot]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Algebra.";
  add_coq_to context;
  add_NZ_to context;
  add_ring_with_quotient_to context;
  context_add_variable context "a" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "b" ~for_coq:true (left (CBasic "R"));
  context_add_variable context "c" ~for_coq:true (left (CBasic "RquotI"));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Algebra.";
  print_string (string_of_context context);
  ()
