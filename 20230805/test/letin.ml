open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let plus x y = "add @ " ^ x ^ " @ " ^ y in
  let let_plus x y = ("let t = " ^ x ^ " in " ^ (plus "t" y))
  and plus_let x y = ("add @ " ^ x ^ " @ (let t = " ^ y ^ " in t)")
  in
  let let_plus_a_b = let_plus "a" "b", left "(let t := a in ((N.add t) b)): N"
  and let_plus_a_c = let_plus "a" "c", left "(let t := a in ((Z.add (Z.of_N t)) c)): Z"
  and let_plus_c_a = let_plus "c" "a", left "(let t := c in ((Z.add t) (Z.of_N a))): Z"
  and let_plus_a_0 = let_plus "a" "0", left "(let t := a in ((N.add t) 0%N)): N"
  and let_plus_c_0 = let_plus "c" "0", left "(let t := c in ((Z.add t) (Z.of_N 0%N))): Z"
  and let_plus_0_a = let_plus "0" "a", left "(let t := 0%N in ((N.add t) a)): N"
  and let_plus_0_c = let_plus "0" "c", left "(let t := 0%N in ((Z.add (Z.of_N t)) c)): Z"
  and plus_let_a_b = plus_let "a" "b", left "((N.add a) (let t := b in t)): N"
  and plus_let_a_c = plus_let "a" "c", left "((Z.add (Z.of_N a)) (let t := c in t)): Z"
  in
  [let_plus_a_b; let_plus_a_c; let_plus_c_a; let_plus_a_0; let_plus_c_0; let_plus_0_a; let_plus_0_c;
   plus_let_a_b; plus_let_a_c]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Letin.";
  add_coq_to context;
  add_NZ_to context;
  context_add_variable context "a" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "b" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "c" ~for_coq:true (left (CBasic "Z"));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Letin.";
  print_string (string_of_context context);
  ()
