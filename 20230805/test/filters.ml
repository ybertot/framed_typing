open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let v_to = "tends_to @ v",
             left "(tends_to v): (filter N -> (filter R -> Prop))"
  and v_to_x = "tends_to @ v @ +oo @ x",
               left "(((tends_to v) filter_nat) (filter_of_Rbar (Rbar_of_R x))): Prop"
  and v_to_infty = "tends_to @ v @ +oo @ +oo",
                   left "(((tends_to v) filter_nat) (filter_of_Rbar pinfty)): Prop"
  and f_x_to_x = "tends_to @ f @ x @ x",
                 left "(((tends_to f) (filter_of_Rbar (Rbar_of_R x))) (filter_of_Rbar (Rbar_of_R x))): Prop"
  and f_x_to_infty = "tends_to @ f @ x @ +oo",
                     left "(((tends_to f) (filter_of_Rbar (Rbar_of_R x))) (filter_of_Rbar pinfty)): Prop"
  and f_infty_to_x = "tends_to @ f @ +oo @ x",
                     left "(((tends_to f) (filter_of_Rbar pinfty)) (filter_of_Rbar (Rbar_of_R x))): Prop"
  and f_infty_to_1 = "tends_to @ f @ +oo @ 1",
                     left "(((tends_to f) (filter_of_Rbar pinfty)) (filter_of_Rbar (Rbar_of_R (R_of_Z (Z.of_N 1%N))))): Prop"
  and f_infty_to_infty = "tends_to @ f @ +oo @ +oo",
                         left "(((tends_to f) (filter_of_Rbar pinfty)) (filter_of_Rbar pinfty)): Prop"
  in
  [
    v_to; v_to_x; v_to_infty; f_x_to_x; f_x_to_infty;
    f_infty_to_x; f_infty_to_1; f_infty_to_infty
  ]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Filters.";
  add_coq_to context;
  add_NZ_to context;
  add_filters_to context;
  context_add_variable context "x" ~for_coq:true (left (CBasic "R"));
  context_add_variable context "v" ~for_coq:true (left (CFunct (CBasic "N", CBasic "R")));
  context_add_variable context "f" ~for_coq:true (left (CFunct (CBasic "R", CBasic "R")));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Filters.";
  print_string (string_of_context context);
  ()
