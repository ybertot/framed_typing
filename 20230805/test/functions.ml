open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let add_f = "add @f",
              left "(FIKadd f): ((I -> K) -> (I -> K))"
  and add_fg = "add @ f @ g",
               left "((FIKadd f) g): (I -> K)"
  and mul_fg = "mul @ f @ g",
               left "((FIKmul f) g): (I -> K)"
  and add_f0 = "add @ f @ 0",
               left "((FIKadd f) (FIK_of_K (K_of_Z (Z.of_N 0%N)))): (I -> K)"
  and add_fh = "add @ f @ h",
               left "((FIKadd f) ((compose K_of_Z) ((compose Z.of_N) h))): (I -> K)"
  and mul_kf = "mul @ k @ f",
               left "((FIKext_mul k) f): (I -> K)"
  in
  [add_f; add_fg; mul_fg; add_f0; add_fh; mul_kf]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Functions.";
  add_coq_to context;
  add_NZ_to context;
  add_field_to context "K";
  context_add_variable ~for_coq:true context "I" (left (CBasic "Type"));
  add_numeric_functions_on_to context "I" "K";
  context_add_variable context "f" ~for_coq:true (left (CFunct (CBasic "I", CBasic "K")));
  context_add_variable context "g" ~for_coq:true (left (CFunct (CBasic "I", CBasic "K")));
  context_add_variable context "h" ~for_coq:true (left (CFunct (CBasic "I", CBasic "N")));
  context_add_variable context "k" ~for_coq:true (left (CBasic "K"));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Functions.";
  print_string (string_of_context context);
  ()
