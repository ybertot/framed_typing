open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  ["add @ 0 @ (mul @ 0 @ (v @ 0))",
   left "((Eadd E0) ((Eext_mul (K_of_Z (Z.of_N 0%N))) (v 0%N))): E"]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Zero.";
  add_coq_to context;
  add_NZ_to context;
  add_field_to context "K";
  add_vector_space_to context "E" "K";
  context_add_variable context "v" ~for_coq:true (left (CFunct (CBasic "N", CBasic "E")));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Zero.";
  print_string (string_of_context context);
  ()
