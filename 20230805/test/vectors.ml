open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let add_f = "add @ f",
              left "(Fadd f): (F -> F)"
  and mul_k = "mul @ k",
              left "(Kmul k): (K -> K)"
  and add_ff = "add @ f @ f",
               left "((Fadd f) f): F"
  and add_fg = "add @ f @ g",
               left "((Eadd (E_of_F f)) (E_of_G g)): E"
  and add_f0 = "add @ f @ 0",
               left "((Fadd f) F0): F"
  and mul_k0 = "mul @ k @ 0",
               left "((Kmul k) (K_of_Z (Z.of_N 0%N))): K"               
  and mul_kf = "mul @ k @ f",
               left "((Fext_mul k) f): F"
  in
  [add_f; add_ff; add_fg; add_f0; mul_k; mul_k0; mul_kf]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Vectors.";
  add_coq_to context;
  add_NZ_to context;
  add_field_to context "K";
  add_vector_space_to context "E" "K";
  add_subvector_space_to context "F" "E" "K";
  add_subvector_space_to context "G" "E" "K";
  context_add_variable context "f" ~for_coq:true (left (CBasic "F"));
  context_add_variable context "g" ~for_coq:true (left (CBasic "G"));
  context_add_variable context "k" ~for_coq:true (left (CBasic "K"));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Vectors.";
  print_string (string_of_context context);
  ()
