open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let stupid_a_bool = "a: bool",
                      right "[cexpr_coercion] doesn't know how to go from N to bool"
  and plus_a_b = "add @ a @ b",
                 left "((N.add a) b): N"
  and plus_a_c = "add @ a @ c",
                 left "((Z.add (Z.of_N a)) c): Z"
  and plus_c_1 = "add @ c @ 1",
                 left "((Z.add c) (Z.of_N 1%N)): Z"
  and not_foo = "fun x |-> not @ x",
                left "(fun x => (not x)): (Prop -> Prop)"
  and lambda_x_plus_1 = "fun x |-> add @ x @ -1",
                        left "(fun x => ((Z.add (Z.of_N x)) (-1)%Z)): (N -> Z)"
  and plus_a_annot = "(add @ a): Z -> Z",
                     left "(Z.add (Z.of_N a)): (Z -> Z)"
  and plus_c = "add @ c",
               left "(Z.add c): (Z -> Z)"
  and plus_a = "add @ a",
               left "(N.add a): (N -> N)"
  and plus_a_b_c = "add @ (add @ a @ b) @ c",
                   left "((Z.add (Z.of_N ((N.add a) b))) c): Z"
  and plus_two_vars = "fun x |-> (fun y |-> add @ x @ y)",
                      left "(fun x => (fun y => ((N.add x) y))): (N -> (N -> N))"
  in
  [stupid_a_bool; plus_a_b; plus_a_c; plus_c_1; not_foo;
   lambda_x_plus_1; plus_a_annot; plus_c; plus_a;
   plus_a_b_c; plus_two_vars]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Basic.";
  add_coq_to context;
  add_NZ_to context;
  context_add_variable context "a" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "b" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "c" ~for_coq:true (left (CBasic "Z"));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Basic.";
  print_string (string_of_context context);
  ()
