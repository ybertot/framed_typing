open Either
open List

open Elaboration.Coq
open Elaboration.Context

open Test_tools

let uexprs_to_test =
  let cons_NZ = "cons @ 1 @ nil",
                left "((cons 1%N) nil): list N"
  and cons_N = "cons @ a @ nil",
               left "((cons a) nil): list N"
  and cons_Z = "cons @ -1 @ nil",
               left "((cons (-1)%Z) nil): list Z"
  and list_N = "cons @ 1 @ (cons @ 2 @ nil)",
               left "((cons 1%N) ((cons 2%N) nil)): list N"
  and list_Z1 = "cons @ -1 @ (cons @ 2 @ nil)",
                left "((cons (-1)%Z) ((cons (Z.of_N 2%N)) nil)): list Z"
  and list_Z2 = "cons @ 1 @ (cons @ -2 @ nil)",
                left "((cons (Z.of_N 1%N)) ((cons (-2)%Z) nil)): list Z"
  and stupid_cons_a_a = "cons @ a @ a",
                        right "[elaborate_bottom_top_pass] impossible application of function (cons: { (Z -> (list Z -> list Z)), (N -> (list N -> list N)) } a: { N, Z }): { (list Z -> list Z), (list N -> list N) } to the argument a: { N, Z }"
  and tail = "fun tl |-> cons @ a @ tl",
             left "(fun tl => ((cons a) tl)): (list N -> list N)"
  and eq3_abc = "eq3 @ a @ b @ c",
                left "(((eq3 (Z.of_N a)) (Z.of_N b)) c): bool"
  in
  [cons_NZ; cons_N; cons_Z; list_N; list_Z1; list_Z2;
   stupid_cons_a_a; tail; eq3_abc]

let _ =
  let context = context_new () in
  add_coq_preamble context;
  context_add_coq_line context "Section Polymorphic.";
  add_coq_to context;
  add_NZ_to context;
  context_add_variable context "a" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "b" ~for_coq:true (left (CBasic "N"));
  context_add_variable context "c" ~for_coq:true (left (CBasic "Z"));
  context_add_variable context "eq3" ~for_coq:true (left (CFunct (CBruijn 0, CFunct(CBruijn 0, CFunct (CBruijn 0, CBasic "bool")))));
  test_uexpr_list context uexprs_to_test;
  context_add_coq_line context "End Polymorphic.";
  print_string (string_of_context context);
  ()
