open Either
open Option

open Elaboration.Coq
open Elaboration.Context
open Elaboration.Elaborate

let uexpr_of_string str =
  let lexbuf = Sedlexing.Utf8.from_string str in
  let lexer = Sedlexing.with_tokenizer Elaboration.Uexpr_lexer.token lexbuf in
  let parser = MenhirLib.Convert.Simplified.traditional2revised Elaboration.Uexpr_parser.main in
  parser lexer

let add_coq_preamble context =
  context_add_coq_line context "Require Import ZArith Coq.Program.Basics.";
  context_add_coq_line context "Set Implicit Arguments.";
  context_add_coq_line context "Unset Strict Implicit.";
  context_add_coq_line context "Unset Printing Implicit Defensive.";
  context_add_coq_line context ""

let add_coq_to context =
  let add_s symbol_name name level ctype =
    context_add_symbol context symbol_name level (CConst (name, ctype))
  and add name ctype =
    context_add_variable context name (left ctype)
  and bool = CBasic "bool"
  and prop = CBasic "Prop"
  and n = CBasic "N"
  and z = CBasic "Z"
  in
  let add_compose =
    add "compose" (CFunct ((CFunct (CBruijn 1, CBruijn 2)),
                           CFunct ((CFunct (CBruijn 0, CBruijn 1)),
                                   (CFunct (CBruijn 0, CBruijn 2)))))
  and add_bool =
    add "is_true" (ctype_predicate bool);
    add "True" prop;
    add "False" prop;
    add "and" (ctype_relation prop);
    add "or" (ctype_relation prop);
    add "not" (ctype_predicate prop)
  and add_N =
    add "N.add" (ctype_operator n);
    add "N.mul" (ctype_operator n);
    add_s "add" "N.add" n (ctype_operator n);
    add_s "mul" "N.mul" n (ctype_operator n)
  and add_Z =
    add "Z.add" (ctype_operator z);
    add "Z.mul" (ctype_operator z);
    add "Z.of_N" (ctype_funct n z);
    add_s "add" "Z.add" z (ctype_operator z);
    add_s "mul" "Z.mul" z (ctype_operator z)
  and add_list =
    let a_list = CParam ("list", [CBruijn 0]) in
    add "nil" a_list;
    add "cons" (CFunct(CBruijn 0, ctype_involution a_list))
  in
  add_compose;
  add_bool;
  add_N;
  add_Z;
  add_list

let add_NZ_to context =
  let n_parser value =
    if Str.string_match (Str.regexp "[0-9]+$") value 0
    then
      some (CConst (value ^ "%N", CBasic "N"))
    else none
  and z_parser value =
    if Str.string_match (Str.regexp "-?[0-9]+$") value 0 then
      begin
        if String.starts_with ~prefix:"-" value
        then some (CConst ("(" ^ value ^ ")%Z", CBasic "Z"))
        else some (CConst (value ^ "%Z", CBasic "Z"))
      end
    else none
  in
  context_add_constant_parser context n_parser;
  context_add_constant_parser context z_parser;
  context_add_coercion context "Z.of_N" (CBasic "N") (CBasic "Z")

let add_field_to context name =
  let add name ctype = context_add_variable ~for_coq:true context name (left ctype)
  and kk = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator kk);
  add (name ^ "mul") (ctype_operator kk);
  context_add_symbol context "add" kk (CConst (name ^ "add", ctype_operator kk));
  context_add_symbol context "mul" kk (CConst (name ^ "mul", ctype_operator kk));
  add (name ^ "_of_Z") (CFunct (CBasic "Z", kk));
  context_add_coercion context (name ^ "_of_Z") (CBasic "Z") kk

let add_vector_space_to context name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (left ctype)
  and kk = CBasic kk_name
  and ee = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator ee);
  add (name ^ "ext_mul") (CFunct (kk, CFunct (ee, ee)));
  context_add_symbol context "add" ee (CConst (name ^ "add", ctype_operator ee));
  context_add_symbol context "mul" ee (CConst (name ^ "ext_mul", CFunct (kk, CFunct (ee, ee))));
  add (name ^ "0") ee;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst (name ^ "0", ee))
                                                    else none)

let add_subvector_space_to context name ee_name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (left ctype)
  and kk = CBasic kk_name
  and ee = CBasic ee_name
  and ff = CBasic name
  in
  add name (CBasic "Type");
  add (name ^ "add") (ctype_operator ff);
  add (name ^ "ext_mul") (CFunct (kk, CFunct (ff, ff)));
  add (ee_name ^ "_of_" ^ name) (CFunct (ff, ee));
  context_add_symbol context "add" ff (CConst (name ^ "add", ctype_operator ff));
  context_add_symbol context "mul" ff (CConst (name ^ "ext_mul", CFunct (kk, CFunct (ff, ff))));
  add (name ^ "0") ff;
  context_add_coercion context (ee_name ^ "_of_" ^ name) ff ee;
  context_add_constant_parser context (fun value -> if value = "0"
                                                    then some (CConst (name ^ "0", ff))
                                                    else none)

let add_numeric_functions_on_to context name kk_name =
  let add name ctype =
    context_add_variable ~for_coq:true context name (left ctype)
  and kk = CBasic kk_name
  and fik = CFunct (CBasic name, CBasic kk_name)
  in
  add ("F" ^ name ^ kk_name ^ "add") (ctype_operator fik);
  add ("F" ^ name ^ kk_name ^ "mul") (ctype_operator fik);
  add ("F" ^ name ^ kk_name ^ "ext_mul") (CFunct (kk, CFunct (fik, fik)));
  add ("F" ^ name ^ kk_name ^ "_of_" ^ kk_name) (CFunct (kk, fik));
  context_add_symbol context "add" fik (CConst ("F" ^ name ^ kk_name ^ "add", ctype_operator fik));
  context_add_symbol context "mul" fik (CConst ("F" ^ name ^ kk_name ^ "ext_mul", CFunct (kk, CFunct (fik, fik))));
  context_add_symbol context "mul" fik (CConst ("F" ^ name ^ kk_name ^ "mul", CFunct (fik, CFunct (fik, fik))));
  context_add_coercion context ("F" ^ name ^ kk_name ^ "_of_" ^ kk_name) kk fik

let add_filters_to context =
  let add name ctype =
    context_add_variable ~for_coq:true context name (left ctype)
  and n = CBasic "N"
  and z = CBasic "Z"
  and r = CBasic "R"
  and rbar = CBasic "Rbar"
  and filter_n = CParam ("filter", [CBasic "N"])
  and filter_r = CParam ("filter", [CBasic "R"])
  in
  add "filter" (CFunct (CBasic "Type", CBasic "Type"));
  add "tends_to" (CFunct (CFunct (CBruijn 0, CBruijn 1),
                          CFunct (CParam ("filter", [CBruijn 0]),
                                  CFunct (CParam ("filter", [CBruijn 1]),
                                          CBasic "Prop"))));
  add "R" (CBasic "Type");
  add "Rbar" (CBasic "Type");
  add "pinfty" rbar;
  add "minfty" rbar;
  add "R_of_Z" (CFunct (z, r));
  context_add_coercion context "R_of_Z" z r;
  add "Rbar_of_R" (CFunct (r, rbar));
  context_add_coercion context "Rbar_of_R" r rbar;
  add "filter_of_Rbar" (CFunct (rbar, filter_r));
  context_add_coercion context "filter_of_Rbar" rbar filter_r;
  add "filter_nat" filter_n;
  context_add_symbol context "+oo" n (CConst ("filter_nat", filter_n));
  context_add_symbol context "+oo" r (CConst ("pinfty", rbar));
  context_add_symbol context "-oo" r (CConst ("minfty", rbar));
  ()

let add_ring_with_quotient_to context =
  let add name ctype = context_add_variable ~for_coq:true context name (left ctype)
  and r = CBasic "R"
  and rquoti = CBasic "RquotI"
  in
  add_field_to context "R";
  add "RquotI" (CBasic "Type");
  add ("RquotIadd") (ctype_operator rquoti);
  add ("RquotImul") (ctype_operator rquoti);
  context_add_symbol context "add" rquoti (CConst ("RquotIadd", ctype_operator rquoti));
  context_add_symbol context "mul" rquoti (CConst ("RquotImul", ctype_operator rquoti));
  add ("R_modI") (CFunct (r, rquoti));
  context_add_coercion context ("R_modI") r rquoti;
  ()

let test_uexpr_list context =
  let handle (string, expected) =
    let actual =
      try
        let uexpr = uexpr_of_string string in
        let res = elaborate context uexpr in
        let s_res = string_of_cexpr res
        and c_res = string_of_ctype (ctype_of_cexpr res)
        in
        left (s_res ^ ": " ^ c_res)
      with Invalid_argument msg ->
        right msg
    in
    match actual, expected with
    | Left s_actual, Left s_expected ->
       context_add_coq_line context
           ("Check "
            ^ s_actual
            ^ ". "
            ^ (if s_actual = s_expected
               then "(* OK *)"
               else ("(* BAD, expected: " ^ s_expected ^ " *)")))
    | Right s_actual, Left s_expected ->
       context_add_coq_line context
       ("(* BAD expected a result "
          ^ s_expected
          ^ " but got error "
          ^ s_actual
          ^ " *)")
    | Left s_actual, Right s_expected ->
       context_add_coq_line context
       ("Check "
        ^ s_actual
        ^ ". (* maybe BAD, expected "
        ^ s_expected
        ^ " *)")
    | Right s_actual, Right s_expected ->
       if s_actual = s_expected
       then begin
           context_add_coq_line context
             ("(* OK as expected error: "
              ^  s_actual
              ^  " *)")
         end else begin
           context_add_coq_line context
             ("(* BAD expected error "
              ^ s_expected
              ^ " but instead got error "
              ^ s_actual
              ^ " *)")
         end
  in
  List.iter handle
