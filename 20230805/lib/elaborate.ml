open Either
open Option
open List

open Coq
open Untyped
open Fuzzy
open Context

let ftype_choice frame ftype =
  let helper ct1 ct2 =
    if frame_le frame ct1 ct2 then -1
    else if frame_le frame ct2 ct2 then 1
    else 0
  in
  match ftype with
  | Left ctype ->
     ctype
  | Right ctypes ->
     if ctypes = [] then raise (Invalid_argument "[ftype_choice] from an empty type!");
     let sorted_ctypes = stable_sort helper ctypes in
     nth sorted_ctypes 0

let cexpr_coercion context cexpr src dst =
  let compose = CConst ("compose", CFunct ((CFunct (CBruijn 1, CBruijn 2)),
                                           CFunct ((CFunct (CBruijn 0, CBruijn 1)),
                                                   (CFunct (CBruijn 0, CBruijn 2)))))
  and msg src dst =
    "[cexpr_coercion] doesn't know how to go from "
    ^ (string_of_ctype src) ^ " to "
    ^ (string_of_ctype dst)
  in
  let helper_cst cexpr coercion =
    match context_variable_ftype context coercion with
    | Some (Left ctype) ->
       CApp (CConst (coercion, ctype), cexpr)
    | _ ->
       raise (Invalid_argument ("[cexpr_coercion] shouldn't happen"))
  and helper_dst cexpr coercion =
    match context_variable_ftype context coercion with
    | Some (Left ctype) ->
       CApp (CApp (compose, CConst (coercion, ctype)), cexpr)
    | _ ->
       raise (Invalid_argument ("[cexpr_coercion] shouldn't happen"))
  and helper_src cexpr coercion =
    match context_variable_ftype context coercion with
    | Some (Left ctype) ->
       CApp (CApp (compose, cexpr), CConst (coercion, ctype))
    | _ ->
       raise (Invalid_argument ("[cexpr_coercion] shouldn't happen"))
  in
  if is_some (ctype_unify src dst)
  then
    cexpr
  else
    try
      let path = frame_coercion_path context.frame src dst in
      fold_left helper_cst cexpr path
    with Not_found ->
      match src, dst with
      | CFunct (arg_src, res_src), CFunct (arg_dst, res_dst) ->
         begin
           try
             let path_src = frame_coercion_path context.frame arg_dst arg_src in
             let cexpr_src = fold_left helper_src cexpr path_src in
             begin
               try
                 let path_dst = frame_coercion_path context.frame res_src res_dst in
                 fold_left helper_dst cexpr_src path_dst
               with Not_found ->
                 raise (Invalid_argument (msg res_src res_dst))
             end
           with Not_found ->
             raise (Invalid_argument (msg arg_dst arg_src))
         end
      | _, _ ->
         raise (Invalid_argument (msg src dst))

let elaborate_impose context fexpr used =
  let msg branch =
    "[elaborate_impose] "
    ^ branch
    ^ ": "
    ^ (string_of_fexpr fexpr)
    ^ " is used as "
    ^ (string_of_ftype used)
    ^ " which is incoherent"
  in
  match fexpr with
  | FCoq (cexpr, known) ->
     begin
       let tight = frame_ftype_tighten context.frame known used in
       if tight = right [] then
         begin
           let src = ctype_of_cexpr cexpr
           and dst = ftype_choice context.frame used
           in
           let coerced = cexpr_coercion context cexpr src dst in
           FCoq (coerced, used)
         end
       else
         FCoq (cexpr, tight)
     end
  | FVar (name, known) ->
     begin
       let tight_pre = frame_ftype_tighten context.frame known used in
       match context_variable_ftype context name with
       | Some (Left ctype) ->
          FCoq (CConst (name, ctype), tight_pre)
       | Some (Right ctypes) ->
          let tight_post = frame_ftype_tighten context.frame (right ctypes) tight_pre in
          if tight_post = Right [] then
            raise (Invalid_argument ("[elaborate_impose] variable "
                                     ^ name
                                     ^ " has incohent typings: "
                                     ^ "context says "
                                     ^ (string_of_ftype (right ctypes))
                                     ^ " known as "
                                     ^ (string_of_ftype known)
                                     ^ " and used as "
                                     ^ (string_of_ftype used)));
          context_update_variable context name tight_post;
          FVar (name, tight_post)
       | None ->
          raise (Invalid_argument ("[elaborate_impose] variable `"
                                   ^ name
                                   ^ " isn't known in the context"))
     end
  | FSymb (symbol, known) ->
     let ftype = frame_ftype_tighten context.frame known used in
     if ftype = right [] then
       raise (Invalid_argument (msg "symbol"));
     let symbol = symbol_tighten context.frame symbol ftype in
     if symbol_is_empty symbol
     then
       raise (Invalid_argument ("[elaborate_impose] symbol `"
                                ^ (symbol_name symbol)
                                ^ "` doesn't have any interpretation as a "
                                ^ (string_of_ftype ftype)))
     else if symbol_is_definite symbol
     then
       let cexpr = symbol_first_instance symbol in
       FCoq (cexpr, ftype)
     else
       FSymb (symbol, ftype)
  | FApp (fnct, arg, known) ->
     let ftype = frame_ftype_tighten context.frame known used in
     if ftype = right [] then
       raise (Invalid_argument (msg "function application"));
     FApp (fnct, arg, ftype)
  | FLambda (name, ftype, body, body_known) ->
     let body_ftype = frame_ftype_tighten context.frame body_known used in
     if body_ftype = right [] then
       raise (Invalid_argument (msg "lambda-expression"));
     FLambda (name, ftype, body, body_ftype)
  | FLet (name, def, def_ftype, body, known) ->
     let ftype = frame_ftype_tighten context.frame known used in
     FLet (name, def, def_ftype, body, ftype)

let fexpr_of_const context name =
  match context_variable_ftype context name with
  | Some ftype ->
     begin
       match ftype with
       | Left ctype ->
          FCoq (CConst (name, ctype), frame_ftype_above_ctype context.frame ctype)
       | Right _ ->
          FVar (name, ftype)
     end
  | None -> (* must be a symbol, then *)
      let symbol = context_parse_symbol context name in
      FSymb (symbol, symbol_ftype context.frame symbol)

let fexpr_of_uexpr context (uexpr: uexpr) =
  let ftype_of_annot annot = fold ~none:(right [CBruijn 0]) ~some:(fun ct -> right [ct]) annot in
  let rec helper uexpr used =
    match uexpr with
    | UString (name, annot) ->
       let pre = fexpr_of_const context name in
       let annot = ftype_of_annot annot in
       let tight = frame_ftype_tighten context.frame annot used in
       if tight = right [] then
         raise (Invalid_argument ("[fexpr_of_uexpr] constant "
                                  ^ name
                                  ^ " is annotated as a "
                                  ^ (string_of_ftype annot)
                                  ^ " but used as a "
                                  ^ (string_of_ftype used)));
       elaborate_impose context pre tight
    | UApp (ufnct, uarg, annot) ->
       let annot = ftype_of_annot annot in
       let fnct = helper ufnct (right [CFunct (CBruijn 0, CBruijn 1)]) in
       let arg = helper uarg (right [CBruijn 0]) in
       FApp (fnct, arg, annot)
    | ULambda (var_name, var_annot, ubody, body_annot) ->
       let var_annot = fold ~none:(right [CBruijn 0]) ~some:left var_annot
       and body_annot = ftype_of_annot body_annot
       in
       context_local_work context (fun () ->
           context_add_variable context var_name var_annot;
           let body_post = helper ubody body_annot in
           let pre =
             FLambda (var_name, var_annot,
                      body_post, ftype_of_fexpr body_post) in
           elaborate_impose context pre used
         )
    | ULet (var_name, udef, def_annot, ubody, body_annot) ->
       let def_ftype = ftype_of_annot def_annot
       and body_ftype = ftype_of_annot body_annot
       in
       let def = helper udef def_ftype in
       context_local_work context (fun () ->
           context_add_variable context var_name def_ftype;
           let body = helper ubody body_ftype in
           let pre = FLet (var_name,
                           def, ftype_of_fexpr def,
                           body, ftype_of_fexpr body)
           in
           elaborate_impose context pre used
         )
  in
  helper uexpr (right [CBruijn 0])

let rec elaborate_bottom_top_pass context fexpr =
  match fexpr with
  | FCoq _  | FSymb _ ->
     fexpr
  | FVar (name, used) ->
     begin
     (* beware: here we have something to do because the variable might
        have been updated in another branch somewhere *)
       let known =
         match
           context_variable_ftype context name
         with
         | Some res ->
            res
         | None ->
            raise (Invalid_argument ("[elaborate_bottom_top_pass] variable `"
                                     ^ name
                                     ^ "` doesn't exist in the context!"))
       in
       let tight = frame_ftype_tighten context.frame known used in
       if tight = Right [] then
         raise (Invalid_argument ("[elaborate_bottom_top_pass] variable `"
                                  ^ name
                                  ^ "` is known to be of type "
                                  ^ (string_of_ftype known)
                                  ^ " and used as "
                                  ^ (string_of_ftype used)
                                  ^ " and it's incoherent"));
       match known with
       | Left ctype ->
          FCoq (CConst (name, ctype), tight)
       | Right _ ->
          FVar (name, tight)
     end
  | FApp (fnct, arg, used) ->
     let fnct_pre = elaborate_bottom_top_pass context fnct
     and arg_pre = elaborate_bottom_top_pass context arg
     in
     let fnct_pre_ftype = ftype_of_fexpr fnct_pre
     and arg_pre_ftype = ftype_of_fexpr arg_pre
     in
     let fnct_ftype, arg_ftype, res_ftype =
       frame_app_ftypes context.frame fnct_pre_ftype arg_pre_ftype used
     in
     if fnct_ftype = right [] || arg_ftype = right [] then
       raise (Invalid_argument ("[elaborate_bottom_top_pass] impossible application of function "
                                ^ (string_of_fexpr fnct_pre)
                                ^ " to the argument "
                                ^ (string_of_fexpr arg_pre)));
     let fnct_post = elaborate_impose context fnct_pre fnct_ftype
     and arg_post = elaborate_impose context arg_pre arg_ftype
     in
     begin
       match fnct_post, arg_post with
       | FCoq (fnct_cexpr, Left fnct_ctype), FCoq (arg_cexpr, Left arg_ctype) ->
            let arg_ctype_expected = ctype_function_source fnct_ctype in
            let arg_coerced = cexpr_coercion context arg_cexpr arg_ctype arg_ctype_expected in
            FCoq (CApp (fnct_cexpr, arg_coerced),
                  frame_ftype_tighten context.frame
                    (left (ctype_function_destination fnct_ctype)) used)
       | _ ->
          FApp (fnct_post, arg_post, res_ftype)
     end
  | FLambda (var_name, var_ftype, body, used) ->
     context_local_work context (fun () ->
         context_add_variable context var_name var_ftype;
         let body = elaborate_bottom_top_pass context body in
         let var_ftype_updated =
           if is_left var_ftype
           then var_ftype
           else
             match context_variable_ftype context var_name with
             | Some res -> res
             | None -> raise (Invalid_argument "[elaborate_bottom_top_pass] shouldn't happen")
         in
         match body, var_ftype_updated with
         | FCoq (cexpr, _), Left var_ctype ->
            FCoq (CLambda (var_name, var_ctype, cexpr), ftype_funct var_ftype_updated used)
         | _ ->
            FLambda (var_name, var_ftype_updated, body, used)
       )
  | FLet (var_name, def_old, def_ftype, body, used) ->
     let def_new = elaborate_bottom_top_pass context def_old in
     let def_ftype_updated =
       match def_new with
       | FCoq (_, def_ftype_) ->
          left (ftype_choice context.frame def_ftype_)
       | _ ->
          frame_ftype_tighten context.frame (ftype_of_fexpr def_new) def_ftype
     in
     context_local_work context (fun () ->
         context_add_variable context var_name def_ftype_updated;
         let body_new = elaborate_bottom_top_pass context body in
         let body_known = ftype_of_fexpr body_new in
         let def_ftype_new =
           match context_variable_ftype context var_name with
           | Some res ->
              frame_ftype_tighten context.frame res def_ftype_updated
           | None ->
              raise (Invalid_argument ("[elaborate_bottom_top_pass] shouldn't happen"))
         in
         begin
           match def_new, body_new with
           | FCoq (def_cexpr, _), FCoq (body_cexpr, body_used) ->
              FCoq (CLet (var_name, def_cexpr, body_cexpr), body_used)
           | _ ->
            FLet (var_name, def_new, def_ftype_new, body_new,
                  frame_ftype_tighten context.frame body_known used)
         end
       )

let rec elaborate_top_bottom_pass context (used: ftype) fexpr =
  match fexpr with
  | FCoq _ | FVar _ | FSymb _ ->
     if used = right [] then raise (Invalid_argument "[elaborate_top_bottom_pass] used is empty");
     elaborate_impose context fexpr used
  | FApp (fn, arg, known) ->
     begin
       let tight = frame_ftype_tighten context.frame known used in
       let fn = elaborate_top_bottom_pass context (ftype_funct_to tight) fn in
       let fn_ftype_pre = ftype_of_fexpr fn in
       let arg_ftype = ftype_function_source fn_ftype_pre in
       let arg = elaborate_top_bottom_pass context arg_ftype arg in
       match fn, arg with
       | FCoq (fnct_cexpr, Left fnct_ctype), FCoq (arg_cexpr, Left arg_ctype) ->
          begin
            let arg_ctype_expected = ctype_function_source fnct_ctype in
            let arg_coerced= cexpr_coercion context arg_cexpr arg_ctype arg_ctype_expected in
            FCoq (CApp (fnct_cexpr, arg_coerced), known) (* FIXME: really known?! *)
          end
       | _ ->
          let fn_ftype_post =
            frame_ftype_tighten context.frame fn_ftype_pre (ftype_funct_from arg_ftype)
          in
          FApp (fn, arg, ftype_function_destination fn_ftype_post)
     end
  | FLambda (var_name, var_ftype, body, body_known) ->
     let var_used = ftype_function_source used
     and body_used = ftype_function_destination used
     in
     let var_ftype_pre = frame_ftype_tighten context.frame var_ftype var_used
     and body_ctypes = frame_ftype_tighten context.frame body_known body_used
     in
     context_local_work context (fun () ->
         context_add_variable context var_name var_ftype_pre;
         let body = elaborate_top_bottom_pass context body_ctypes body in
         let var_ftype_post =
           match context_variable_ftype context var_name with
           | Some res -> res
           | None -> raise (Invalid_argument "[elaborate_top_bottom_pass] shouldn't happen")
         in
         FLambda (var_name, var_ftype_post, body, ftype_of_fexpr body)
       )
  | FLet (var_name, def, def_known, body, body_known) ->
     context_local_work context (fun () ->
         context_add_variable context var_name def_known;
         let body_new = elaborate_top_bottom_pass context used body in
         let body_new_known = ftype_of_fexpr body_new in
         let def_ftype =
           match context_variable_ftype context var_name with
           | Some res -> res
           | None -> raise (Invalid_argument "[elaborate_top_bottom_pass] shouldn't happen")
         in
         let def_new = elaborate_top_bottom_pass context def_ftype def in
         context_update_variable context var_name (ftype_of_fexpr def_new);
         FLet (var_name, def_new,
               frame_ftype_tighten context.frame def_ftype def_known,
               body_new,
               frame_ftype_tighten context.frame body_new_known body_known)
       )

let elaborate_choice context fexpr =
  let rec helper fexpr =
    match fexpr with
    | FCoq (cexpr, ftype) ->
       if is_left ftype
       then  FCoq (cexpr, ftype), false
       else FCoq (cexpr, left (ftype_choice context.frame ftype)), true
    | FVar (name, used) ->
       FVar (name, used), false
    | FSymb (symbol, used) ->
       let cexpr = symbol_first_instance symbol in
       let known_post = frame_ftype_tighten context.frame (right [ctype_of_cexpr cexpr]) used in
       FCoq (cexpr, known_post), true
    | FApp (fnct, arg, known) ->
       let fnct, found = helper fnct in
       if found
       then
           FApp (fnct, arg, known), true
       else
         let arg, found = helper arg in
         FApp (fnct, arg, known), found
    | FLambda (var_name, var_ftype, body, body_used) ->
       context_local_work context (fun () ->
           context_add_variable context var_name var_ftype;
           let body, found = helper body in
           if found
           then
             FLambda (var_name, var_ftype, body, body_used), true
           else
             begin
               match var_ftype with
               | Left _ ->
                  FLambda (var_name, var_ftype, body, body_used), false
               | Right ctypes ->
                  if 0 = length ctypes
                  then
                    raise (Invalid_argument ("[elaborate_choice] variable `"
                                             ^ var_name
                                             ^ "` doesn't have any possible type!"))
                  else
                    FLambda (var_name, left (ftype_choice context.frame var_ftype),
                             body, body_used), true
             end
         )
    | FLet (var_name, def, def_ftype, body, body_ftype) ->
       let def_new, found = helper def in
       if found
       then
         FLet (var_name, def_new, def_ftype, body, body_ftype), true
       else
         context_local_work context (fun () ->
             context_add_variable context var_name def_ftype;
             let body_new, found = helper body in
             if found
             then
               FLet (var_name, def, def_ftype, body_new, body_ftype), true
             else
               FLet (var_name, def, def_ftype, body, body_ftype), false
           )
  in
  match helper fexpr with
  | res, found ->
     if found
     then res
     else raise (Invalid_argument ("[elaborate_choice] no choice was found for "
                                   ^ (string_of_fexpr fexpr)))

let elaborate context uexpr =
  let print_fexpr name fexpr =
    let ambiguities = fexpr_ambiguities fexpr
    and fuzzyness = fexpr_fuzzy_height fexpr
    in
    context_add_coq_line context
      ("\t(* "
       ^ name
       ^ " (ambiguities: "
       ^ (string_of_int ambiguities)
       ^ ", fuzzy height: "
       ^ (string_of_int fuzzyness)
       ^ ") : "
       ^ (string_of_fexpr fexpr)
       ^ " *)")
  in
  let helper_maker name action (fexpr: fexpr) =
    let res = action fexpr in
    let ambiguities = fexpr_ambiguities res
    and fuzzyness = fexpr_fuzzy_height res
    in
    print_fexpr name res;
    res, (ambiguities + fuzzyness) = 0
  in
  let rec helpers_chainer helpers fexpr =
    match helpers with
      [] -> fexpr, false
    | helper :: others ->
       let res, stop = helper fexpr in
       if stop
       then res, true
       else helpers_chainer others res
  in
  let helper_bt = helper_maker "bottom-top" (elaborate_bottom_top_pass context)
  and helper_tb = helper_maker "top-bottom" (elaborate_top_bottom_pass context (right [CBruijn 0]))
  and helper_choice = helper_maker "choice" (elaborate_choice context)
  in
  let helper_3_bttb_1c =
    helpers_chainer [helper_bt; helper_tb;helper_bt; helper_tb; helper_bt; helper_tb; helper_choice]
  in
  let rec insist fexpr =
    let res, stop = helper_3_bttb_1c fexpr in
    if stop
    then elaborate_bottom_top_pass context res
    else insist res
  in
  let fexpr =
    try
      context_add_coq_line context ("\n(* considering untyped expression: "
                                    ^ (string_of_uexpr uexpr)
                                    ^ " *)");
      fexpr_of_uexpr context uexpr
    with Invalid_argument msg ->
      context_add_coq_line context ("\t(* bad: " ^ msg ^ " *)");
      raise (Invalid_argument msg)
  in
  try
    print_fexpr "first step" fexpr;
    let fexpr_refined = insist fexpr in
    match fexpr_refined with
    | FCoq (cexpr, _) ->
       cexpr
    | _ ->
       raise (Invalid_argument "elaboration didn't give a Coq expression!")
  with Invalid_argument msg ->
    context_add_coq_line context ("\t(* bad: " ^ msg ^ " *)");
    raise (Invalid_argument msg)
