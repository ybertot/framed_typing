open Uexpr_parser

let rec token buf =
  match%sedlex buf with
  | Plus (Chars " \t\r") -> token buf
  | eof -> EOF
  | '(' -> LBRACK
  | ')' -> RBRACK
  | ':' -> COLON
  | '@' -> AROBASE
  | "->" -> ARROW
  | "|->" -> MAPSTO
  | "let" -> LET
  | "in" -> IN
  | "fun" -> LAMBDA
  | "=" -> EQUAL
  | Plus ('a'..'z' | 'A'..'Z' | '0'..'9' | Chars "+-*/_.") -> STRING (Sedlexing.Utf8.lexeme buf)
  | _ -> failwith ("Unexpected token " ^ (Sedlexing.Utf8.lexeme buf))
