open Either
open Option
open List

open Tools



(* COQ TYPES *)
(* simplified *)

type ctype = | CBasic of string
             | CBruijn of int
             | CFunct of ctype * ctype (* source destination *)
             | CParam of string * ctype list

let rec ctype_max_Bruijn ct =
  match ct with
  | CBasic _ -> -1
  | CBruijn n -> n
  | CFunct (src, dst) -> max (ctype_max_Bruijn src) (ctype_max_Bruijn dst)
  | CParam (_, params) -> fold_left (fun m param -> max m (ctype_max_Bruijn param)) (-1) params

let string_of_ctype ct =
  let rec dbvars m n =
    if m <= n then
      " db" ^ (string_of_int m) ^ (dbvars (m + 1) n)
    else
      ""
  and params_helper params =
    match params with
    | [] ->
       ""
    | param :: others ->
       (if String.contains param ' ' then " (" ^ param ^ ")" else " " ^ param)
       ^ (params_helper others)
  in
  let rec helper = function
    | CBasic name ->
       name
    | CBruijn n ->
       "db" ^ (string_of_int n)
    | CFunct (arg, result) ->
       "(" ^ (helper arg) ^ " -> " ^ (helper result) ^ ")"
    | CParam (name, args) ->
       name ^ (params_helper (map helper args))
  in
  let n = ctype_max_Bruijn ct in
  (if n >= 0 then "forall" ^ (dbvars 0 n) ^ ", " else "") ^ (helper ct)

let rec ctype_Bruijn_shift ?(above=0) n = function
  | CBasic name -> CBasic name
  | CBruijn m -> if m >= above then CBruijn (m + n) else CBruijn m
  | CFunct (src, dst) -> CFunct (ctype_Bruijn_shift ~above n src,
                                 ctype_Bruijn_shift ~above n dst)
  | CParam (name, params) -> CParam (name, map (ctype_Bruijn_shift ~above n) params)

let rec ctype_Bruijn_permute m n = function
  | CBasic name -> CBasic name
  | CBruijn s ->
     if s = m then CBruijn n
     else if s = n then CBruijn m
     else CBruijn s
  | CFunct (src, dst) -> CFunct (ctype_Bruijn_permute m n src, ctype_Bruijn_permute m n dst)
  | CParam (name, params) -> CParam (name, map (ctype_Bruijn_permute m n) params)

let ctype_normalize ctype =
  let tab = ref [] in
  let read value =
    match assoc_opt value !tab with
    | Some res -> res
    | None ->
       let res = length !tab in
       tab := (value, res) :: !tab;
       res
  in
  let rec helper = function
    | CBasic value -> CBasic value
    | CBruijn value -> CBruijn (read value)
    | CFunct (arg_, res_) ->
       (* BEWARE: not written just CFunct (helper arg, helper res)
          because the order is important! *)
       let arg = helper arg_ in
       let res = helper res_ in
       CFunct (arg, res)
    | CParam (name, args) -> CParam (name, map helper args)
  in
  helper ctype

let ctype_subst n other orig =
  let rec helper =
    function
    | CBasic name -> CBasic name
    | CBruijn m -> if m = n then other else CBruijn m
    | CFunct (src, dst) -> CFunct (helper src, helper dst)
    | CParam (name, params) -> CParam (name, map helper params)
  in
  ctype_normalize (helper (ctype_Bruijn_shift ~above:(n+1) (1 + ctype_max_Bruijn other) orig))

let ctype_function_source ct =
  match ct with
  | CFunct (src, _) -> src
  | _ -> CBruijn 0

let ctype_function_destination ct =
  match ct with
  | CFunct (_, res) -> ctype_normalize res
  | _ -> CBruijn 0

let ctype_predicate ct = CFunct (ct, CBasic "Prop")
let ctype_relation ct = CFunct (ct, ctype_predicate ct)
let ctype_involution ct = CFunct (ct, ct)
let ctype_operator ct = CFunct (ct, ctype_involution ct)
let ctype_funct arg res = CFunct (arg, ctype_Bruijn_shift ((ctype_max_Bruijn arg)+1) res)
let ctype_funct2 arg1 arg2 res = ctype_funct arg1 (ctype_funct arg2 res)
let ctype_funct3 arg1 arg2 arg3 res = ctype_funct arg1 (ctype_funct2 arg2 arg3 res)
let ctype_funct_from ct = CFunct (ct, CBruijn ((ctype_max_Bruijn ct)+1))
let ctype_funct_to ct = CFunct (CBruijn 0, ctype_Bruijn_shift 1 ct)

let rec ctype_unify ct1 ct2 =
  let module M = struct exception Discovery of ((int * ctype), (int * ctype)) Either.t end in
  let rec helper link recht =
    match link, recht with
    | CBasic a, CBasic b ->
       if a = b
       then some (CBasic a)
       else none
    | CBruijn nl, CBruijn nr ->
       if nl < nr then raise (M.Discovery (left (nl, CBruijn nr)))
       else if nr < nl then raise (M.Discovery (right (nr, CBruijn nl)))
       else some (CBruijn nl)
    | CBruijn nl, other ->
       raise (M.Discovery (left (nl, other)))
    | other, CBruijn nr ->
       raise (M.Discovery (right (nr, other)))
    | CFunct (argl, resl), CFunct (argr, resr) ->
       let* arg = helper argl argr in
       let* res = helper resl resr in
       some (CFunct (arg, res))
    | CParam (namel, argsl), CParam (namer, argsr) ->
       if namel <> namer
       then none
       else
         let* args = my_option_map2 helper argsl argsr in
         some (CParam (namel, args))
    | _ ->
       none
  in
  try
    let* pre_res = helper ct1 ct2 in
    some (ctype_normalize pre_res)
  with M.Discovery exn ->
        match exn with
        | Left (n, other) ->
           let ct1_new = ctype_subst n other ct1 in
           ctype_unify ct1_new ct2
        | Right (n, other) ->
           let ct2_new = ctype_subst n other ct2 in
           ctype_unify ct1 ct2_new



(* COQ EXPRESSIONS *)

type cexpr = | CConst of string * ctype
             | CApp of cexpr * cexpr
             | CLambda of string * ctype * cexpr
             | CLet of string * cexpr * cexpr

let string_of_cexpr cexpr_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper cexpr =
    match cexpr with
    | CConst (name, _) ->
       add_string name
    | CApp (fn, arg) ->
       add_string "(";
       helper fn;
       add_string " ";
       helper arg;
       add_string ")"
    | CLambda (var, _, body) ->
       add_string "(fun ";
       add_string var;
       add_string " => ";
       helper body;
       add_string ")"
    | CLet (var, def, body) ->
       add_string "(let ";
       add_string var;
       add_string " := ";
       helper def;
       add_string " in ";
       helper body;
       add_string ")"
  in
  helper cexpr_;
  Buffer.contents buf

let rec ctype_of_cexpr = function
  | CConst (_, ctype) ->
     ctype
  | CApp (fn, arg) ->
     begin
       let fn_ct_pre = ctype_of_cexpr fn
       and arg_ct = ctype_of_cexpr arg
       in
       match ctype_unify fn_ct_pre (ctype_funct_from arg_ct) with
       | Some fn_ct_post ->
          ctype_function_destination fn_ct_post
       | None ->
          raise (Invalid_argument "[ctype_of_cexpr] FIXME")
     end
  | CLambda (_, arg_ctype, body) ->
     CFunct (arg_ctype, ctype_of_cexpr body)
  | CLet (_, _, body) ->
     ctype_of_cexpr body (* FIXME: doubtful *)
