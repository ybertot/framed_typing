open Coq

(* UNTYPED EXPRESSIONS *)
(* untyped, but with possible annotations *)

type uexpr = | UString of string * ctype option
             | UApp of uexpr * uexpr * ctype option
             | ULambda of string * ctype option * uexpr * ctype option
             | ULet of string * uexpr * ctype option * uexpr * ctype option

let string_of_uexpr ue_ =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec s_of_ue_helper = function
    | UString (name, oannot) ->
       begin match oannot with
       | None ->
          add_string name
       | Some annot ->
          add_string "(";
          add_string name;
          add_string ": ";
          add_string (string_of_ctype annot);
          add_string ")"
       end
    | UApp (fn, arg, oannot) ->
       begin match oannot with
       | None ->
          s_of_ue_helper fn;
          add_string " ";
          s_of_ue_helper arg
       | Some annot ->
          add_string "(";
          s_of_ue_helper fn;
          add_string " ";
          s_of_ue_helper arg;
          add_string ": ";
          add_string (string_of_ctype annot);
          add_string ")"
       end
    | ULambda (name, oannotname, body, oannotbody) ->
       add_string "[";
       add_string name;
       begin match oannotname with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_ctype annot)
       end;
       add_string " |-> ";
       begin match oannotbody with
       | None ->
          s_of_ue_helper body
       | Some annot ->
          add_string "(";
          s_of_ue_helper body;
          add_string "): ";
          add_string (string_of_ctype annot)
       end;
       add_string "]"
    | ULet (name, def, oannotdef, body, oannotbody) ->
       add_string "let ";
       add_string name;
       add_string " = ";
       s_of_ue_helper def;
       begin match oannotdef with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_ctype annot)
       end;
       add_string " in ";
       s_of_ue_helper body;
       begin match oannotbody with
       | None ->
          ()
       | Some annot ->
          add_string ": ";
          add_string (string_of_ctype annot)
       end;
  in
  s_of_ue_helper ue_;
  Buffer.contents buf

let uapp_none fn arg = UApp (fn, arg, None)
let uapp2_none fn arg1 arg2 = uapp_none (uapp_none fn arg1) arg2
let uapp3_none fn arg1 arg2 arg3 = uapp_none (uapp2_none fn arg1 arg2) arg3

let ulet_none name def body = ULet (name, def, None, body, None)
