%token EOF
%token AROBASE
%left AROBASE
%token LBRACK RBRACK COLON
%token ARROW MAPSTO LET IN LAMBDA EQUAL
%token<string> STRING

%{
    open Untyped
    
    let uexpr_impose ue_ ctype =
      match ue_ with
      | UString (str, _) -> UString (str, Some ctype)
      | UApp (fnct, arg, _) -> UApp (fnct, arg, Some ctype)
      | ULambda (name, octype, expr, _) -> ULambda (name, octype, expr , Some ctype)
      | ULet (name, uexpr, octype, expr, _) -> ULet (name, uexpr, octype, expr, Some ctype)

%}

%type <uexpr>main
%start main

%%

main: uexpr EOF { $1 }

uexpr:
  | LBRACK uexpr RBRACK { $2 }
  | uexpr COLON ctype { uexpr_impose $1 $3 }
  | uexpr AROBASE uexpr { UApp ($1, $3, None) }
  | LAMBDA STRING MAPSTO uexpr { ULambda ($2, None, $4, None) }
  | LAMBDA STRING COLON ctype MAPSTO uexpr { ULambda ($2, Some $4, $6, None) }
  | LET STRING EQUAL uexpr IN uexpr { ULet ($2, $4, None, $6, None) }
  | STRING { UString ($1, None) }

ctype:
  | STRING { CBasic $1 }
  | LBRACK ctype RBRACK { $2 }
  | ctype ARROW ctype {  CFunct ($1, $3) }
