open Option
open List

open Untyped
open Fuzzy
open Context

(* ELABORATION CODE *)

let fexpr_coercion fexpr frame src dst =
  let helper fexpr name =
    FApp (FVar (name, noose_default),
          fexpr, noose_of_ftype dst)
  in
  fold_left helper fexpr (frame_coercion_path frame src dst)

let elaborate_impose context imposed fexpr =
  match fexpr with
  | FVar (name, known) ->
     let (_, above) as noose = noose_tighten context.frame known imposed in
     if above = FBottom
     then
       raise (Invalid_argument ("variable expression `"
                                ^ (string_of_fexpr fexpr)
                                ^ "` is known to be "
                                ^ (string_of_noose known)
                                ^ " but is used as "
                                ^ (string_of_noose imposed)))
     else
       begin
         if context_has_variable context name
         then context_variable_noose_tighten context name noose;
         FVar (name, noose)
       end
  | FSymb (symbol_, known) ->
     begin
       let noose = noose_tighten context.frame known imposed in
       let symbol = symbol_tighten context.frame symbol_ noose in
       if symbol_is_empty symbol
       then
         raise (Invalid_argument ("symbol named `"
                                  ^ (symbol_name symbol)
                                  ^ "` doesn't have an interpretation "
                                  ^ (string_of_noose noose)))
       else if symbol_count_ambiguities symbol = 1
       then
         fexpr_of_cexpr (symbol_first_instance symbol)
       else
         FSymb (symbol, symbol_noose context.frame symbol)
     end
  | FApp (fn, arg, known) ->
       let noose = noose_tighten context.frame known imposed in
       FApp (fn, arg, noose)
  | FLambda (var, noose_var, body, noose_body) ->
     begin
       let (below_imposed, above_imposed) = imposed in
       let above_var, below_res =
         match below_imposed with
         | FFunct (above_var, below_res) -> below_res, above_var
         | _ -> FTop, FBottom
       and below_var, above_res =
         match above_imposed with
         | FFunct (below_var, above_res) -> below_var, above_res
         | _ -> FBottom, FTop
       in
       FLambda (var, noose_tighten context.frame noose_var (below_var, above_var),
                body, noose_tighten context.frame noose_body (below_res, above_res))
     end
  | FLet (name, expr, body, noose) ->
     FLet (name, expr, body, noose_tighten context.frame noose imposed)

let rec fexpr_of_uexpr context uexpr noose =
  match uexpr with
  | UConst (name, annot) ->
     let annot_noose = value ~default:noose_default annot in
     let imposed = noose_tighten context.frame noose annot_noose in
     if context_has_variable context name then
       begin
         let var_noose = value ~default:noose_default (context_variable_noose context name) in
         let (_, above) as noose = noose_tighten context.frame imposed var_noose in
         if above = FBottom
         then raise (Invalid_argument ("variable `"
                                       ^ name
                                       ^ "` is declared as "
                                       ^ (string_of_noose var_noose)
                                       ^ " which is not compatible with its use as "
                                       ^ (string_of_noose imposed)))
         else
           context_variable_noose_tighten context name noose;
           FVar (name, noose)
       end
     else (* not a variable, hence a symbol *)
       begin
         let symbol = symbol_tighten context.frame (context_parse_symbol context name) annot_noose in
         if symbol_is_empty symbol
         then
           raise (Invalid_argument ("Symbol `"
                                    ^ name
                                    ^ "` doesn't have an interpretation as "
                                    ^ (string_of_noose annot_noose)))
         else
           FSymb (symbol, symbol_noose context.frame symbol)
       end
  | UApp (fn, arg, annot) ->
     let annot_noose = value ~default:noose_default annot in
     let (below, above) as context_noose = noose_tighten context.frame noose annot_noose in
     let fn_noose = (FFunct (FTop, below), FFunct (FBottom, above)) in
     FApp (fexpr_of_uexpr context fn fn_noose,
           fexpr_of_uexpr context arg noose_default,
           context_noose)
  | ULambda (var, annot_var, body, annot_body) ->
     let noose_var = value ~default:noose_default annot_var
     and noose_body = value ~default:noose_default annot_body in
     let noose_body_tight = noose_tighten context.frame noose_body noose in
     context_local_work context
       (fun () ->
         context_add_variable context var noose_var;
         FLambda (var, noose_var,
                  fexpr_of_uexpr context body noose_body_tight,
                  noose_body_tight))
  | ULet (name, expr, annot_expr, body_, annot_body) ->
     let noose_expr = value ~default:noose_default annot_expr
     and noose_body = value ~default:noose_default annot_body
     in
     context_local_work context
       (fun () ->
         context_add_variable context name noose_expr;
         FLet (name, fexpr_of_uexpr context expr noose_expr,
               fexpr_of_uexpr context body_ noose_body,
               noose_tighten context.frame noose_body noose))

let rec elaborate_top_bottom_pass context imposed fexpr =
  match fexpr with
  | FVar _ | FSymb _ ->
     elaborate_impose context imposed fexpr
  | FApp (fn, arg, noose) ->
     let (below, above) as noose = noose_tighten context.frame noose imposed in
     if above = FBottom
     then
       raise (Invalid_argument ("expression `"
                                ^ (string_of_fexpr fexpr)
                                ^ "` is known to be "
                                ^ (string_of_noose (noose_of_fexpr fexpr))
                                ^ " but is used as "
                                ^ (string_of_noose noose)))
     else
       begin
         let fn_noose = (FFunct (FTop, below), FFunct (FBottom, above)) in
         let fn_new = elaborate_top_bottom_pass context fn_noose fn in
         let (fn_new_below, _) = noose_of_fexpr fn_new in
         let arg_above =
           match fn_new_below with
           | FFunct (arg_above, _) -> arg_above
           | _ -> FTop
         in
         let arg_new = elaborate_top_bottom_pass context (FBottom, arg_above) arg in
         FApp (fn_new, arg_new, (below, above))
       end
  | FLambda (var, noose_var, body, body_noose) ->
     let body_imposed =
       match imposed with
       | FFunct (_, body_below), FFunct (_, body_above) ->
          (body_below, body_above)
       | _ -> (FBottom, FTop)
     in
     let noose = noose_tighten context.frame body_noose body_imposed in
     if snd noose = FBottom then
       Printf.eprintf "fexpr= %s noose=%s body_noose=%s imposed=%s\n" (string_of_fexpr fexpr) (string_of_noose noose) (string_of_noose body_noose) (string_of_noose imposed);
     context_local_work context (fun () ->
         context_add_variable context var noose_var;
         let body_new = elaborate_top_bottom_pass context noose body in
         FLambda (var, value ~default:noose_var (context_variable_noose context var),
                  body_new, noose_of_fexpr body_new))
  | FLet (var, expr, body, noose) ->
     context_local_work context (fun () ->
         context_add_variable context var (noose_of_fexpr expr);
         let body_new = elaborate_top_bottom_pass context noose body in
         let noose_expr = value ~default:noose_default (context_variable_noose context var) in
         FLet (var, elaborate_top_bottom_pass context noose_expr expr,
               body_new, noose_of_fexpr body_new))

let rec elaborate_bottom_top_pass context fexpr =
  match fexpr with
  | FVar _ ->
     fexpr
  | FSymb _ ->
     fexpr (* FIXME: nothing to do: am I that sure? *)
  | FApp (fn_, arg_, noose) ->
     begin
       let fn = elaborate_bottom_top_pass context fn_
       and arg = elaborate_bottom_top_pass context arg_
       in
       let (below_fn, above_fn) = noose_of_fexpr fn
       and (below_arg, _) = noose_of_fexpr arg
       in
       let above_fn_arg, below_fn_res =
         match below_fn with
         | FFunct (arg_above, res_below) -> arg_above, res_below
         | _ -> FTop, FBottom
       and above_fn_res =
         match above_fn with
         | FFunct (_, res_above) -> res_above
         | _ -> FTop
       in
       let noose_fn_revised = (FBottom,
                               FFunct (below_arg, FTop))
       in
       FApp (elaborate_impose context noose_fn_revised fn,
             elaborate_impose context (FBottom, above_fn_arg) arg,
             noose_tighten context.frame noose (below_fn_res, above_fn_res))
     end
  | FLambda (var, noose_var, body_, noose_body) ->
     context_local_work context (fun () ->
         context_add_variable context var noose_var;
         let body = elaborate_bottom_top_pass context body_ in
         let noose_body_new = noose_tighten context.frame (noose_of_fexpr body) noose_body in
         FLambda (var, noose_var, body, noose_body_new))
  | FLet (var, expr_, body_, _) ->
     let expr = elaborate_bottom_top_pass context expr_ in
     context_local_work context (fun () ->
         context_add_variable context var (noose_of_fexpr expr);
         let body = elaborate_bottom_top_pass context body_ in
         FLet (var, expr, body, noose_of_fexpr body))

let elaborate_force_choice context fexpr =
  let rec helper = function
    | FVar (name, noose) ->
       let (below, above) = noose in
       if below = above
       then (* probably a normal context variable *)
         FVar (name, noose), false
       else (* probably a lambda variable *)
         begin
           match context_variable_noose context name with
           | Some (below, above) ->
              FVar (name, (below, above)), false
           | None ->
              raise (Invalid_argument ("variable `"
                                       ^  name
                                       ^ "` disappeared from the context!"))
         end
    | FSymb (symbol, _) ->
       fexpr_of_cexpr (symbol_first_instance symbol), true
    | FApp (fn_, arg_, _) ->
       let fn, found = helper fn_ in
       let noose =
         match noose_of_fexpr fn with
         | FFunct (_, below), FFunct (_, above) -> (below, above)
         | _ -> noose_default
       in
       if found
       then
         FApp (fn, arg_, noose), true
       else
         let arg, found = helper arg_ in
         FApp (fn, arg, noose), found
    | FLambda (var_name, var_noose, body, _) ->
       context_local_work context (fun () ->
           context_add_variable context var_name var_noose;
           let body_new, found = helper body in
           FLambda (var_name, value ~default:var_noose (context_variable_noose context var_name), body_new, noose_of_fexpr body_new), found)
    | FLet (var, expr_, body, body_noose) ->
       let expr, found = helper expr_ in
       if found then
         FLet (var, expr, body, body_noose), true
       else
         context_local_work context (fun () ->
             context_add_variable context var (noose_of_fexpr expr);
             let body_new, found = helper body in
             FLet (var, expr, body_new, noose_of_fexpr body_new), found)
  in
  fst (helper fexpr)

let rec elaborate_coercions context fexpr =
  match fexpr with
  | FVar (name, noose) ->
     let (below, above) = noose in
     if below = above
     then
       FVar (name, noose)
     else
       begin (* reason: it could be the variable of a lambda, and we're fixing it *)
         match context_variable_noose context name with
         | Some (below, above) ->
            if below = above
            then
              FVar (name, noose_of_ftype above)
            else
              raise (Invalid_argument ("variable `"
                                       ^ name
                                       ^ "` still has a fuzzy type"))
         | None ->
            raise (Invalid_argument ("variable `"
                                     ^  name
                                     ^ "` disappeared from the context!"))
       end
  | FSymb (symbol, _) ->
     raise (Invalid_argument ("symbol named `"
                              ^ (symbol_name symbol)
                              ^ "` is still ambiguous"))
  | FApp (fn_, arg_, _) ->
     begin
       let arg = elaborate_coercions context arg_
       and fn = elaborate_coercions context fn_
       in
       let (_, arg_ftype) = noose_of_fexpr arg
       and (_, fn_ftype) = noose_of_fexpr fn in
       match fn_ftype with
       | FFunct (ftype_src, ftype_dst) ->
          begin
            if not (frame_le context.frame arg_ftype ftype_src)
            then
              raise (Invalid_argument ("argument `"
                                       ^ (string_of_fexpr arg)
                                       ^ "` of type `"
                                       ^ (string_of_ftype arg_ftype)
                                       ^ "` isn't compatible with function `"
                                       ^ (string_of_fexpr fn)
                                       ^ "` expecting a `"
                                       ^ (string_of_ftype ftype_src)))
            else
              FApp (fn, fexpr_coercion arg context.frame arg_ftype ftype_src,
                    noose_of_ftype ftype_dst)
          end
       | _ -> raise (Invalid_argument ((string_of_fexpr fn) ^ " isn't a function"))
     end
  | FLambda (var_name, var_noose_, body_, _) ->
     let (_, var_above) = var_noose_ in
     let var_noose = noose_of_ftype var_above in
     context_local_work context (fun () ->
         context_add_variable context var_name var_noose;
         let body = elaborate_coercions context body_ in
         FLambda (var_name, var_noose, body, noose_of_fexpr body))
  | FLet (var, expr_, body_, _) ->
     let expr = elaborate_coercions context expr_ in
     context_local_work context (fun () ->
         context_add_variable context var (noose_of_fexpr expr);
         let body = elaborate_coercions context body_ in
         FLet (var, expr, body, noose_of_fexpr body))
