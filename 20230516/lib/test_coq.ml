open Coq
open Fuzzy
open Context



let add_coq_to context =
  let add_s symbol_name name level ctype =
    context_add_symbol context symbol_name level (CConst (name, ctype))
  and add name ctype =
    context_add_variable context name (noose_of_ftype (ftype_of_ctype ctype))
  and bool = CBasic "bool"
  and prop = CBasic "Prop"
  and n = CBasic "N"
  and z = CBasic "Z"
  in
  let add_bool =
    add "is_true" (CFunct (bool, prop));
    add "True" prop;
    add "False" prop;
    add "and" (ctype_relation prop);
    add "or" (ctype_relation prop);
    add "not" (ctype_predicate prop)
  and add_N =
    add "N.eq" (ctype_relation n);
    add "N.le" (ctype_relation n);
    add "N.add" (ctype_operator n);
    add "N.mul" (ctype_operator n);
    add_s "eq" "N.eq" n (ctype_relation n);
    add_s "add" "N.add" n (ctype_operator n);
    add_s "mul" "N.mul" n (ctype_operator n)
  and add_Z =
    add "Z.eq" (ctype_relation z);
    add "Z.le" (ctype_relation z);
    add "Z.add" (ctype_operator z);
    add "Z.sub" (ctype_operator z);
    add "Z.mul" (ctype_operator z);
    add "Z.of_N" (CFunct (n, z));
    add_s "eq" "Z.eq" z (ctype_relation z);
    add_s "add" "Z.add" z (ctype_operator z);
    add_s "sub" "Z.sub" z (ctype_operator z);
    add_s "mul" "Z.mul" z (ctype_operator z)
  and add_list =
    let cons_of ct = CFunct (ct, CFunct (ctype_list_of ct, ctype_list_of ct)) in
    add "(@nil N)" (ctype_list_of n);
    add "(@cons N)" (cons_of n);
    add "(@nil Z)" (ctype_list_of z);
    add "(@cons Z)" (cons_of z);
    add_s "cons" "(@cons N)" n (cons_of n);
    add_s "nil" "(@nil N)" n (ctype_list_of n);
    add_s "nil" "(@nil Z)" z (ctype_list_of z);
    add_s "cons" "(@cons Z)" z (cons_of z)
  in
  add_bool;
  add_N;
  add_Z;
  add_list
