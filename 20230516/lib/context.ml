open Option
open List

open Tools
open Coq
open Fuzzy

(* VARIABLE LAYERS *)

type layer = {
    mutable variables: noose NotTrie.t;
    mutable coq: Buffer.t;
  }

let layer_new () = { variables = NotTrie.empty;
                     coq = Buffer.create 0;
                   }

let layer_has_variable layer name = NotTrie.mem name layer.variables

let layer_add_variable layer ?(for_coq = false) name noose =
  layer.variables <- NotTrie.update name (fun _ -> some noose) layer.variables;
  if for_coq then
    Buffer.add_string layer.coq ("Variable "
                                 ^ name
                                 ^ ": "
                                 ^ (string_of_noose noose)
                                 ^ ".\n")

let layer_variable_noose layer name = NotTrie.find_opt name layer.variables

let layer_variable_tighten_noose = layer_add_variable (* eh... *)

let string_of_layer ?(for_coq = false) layer =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  if for_coq then begin
      add_string "(* active Coq variables: *)\n\n";
      add_string (Buffer.contents layer.coq)
    end else begin
      add_string "(* layer of fuzzy variables:\n";
      NotTrie.iter (fun name noose ->
          add_string name;
          add_string ": ";
          add_string (string_of_noose noose);
          add_string "\n"
        ) layer.variables;
      add_string "*)\n"
    end;
  Buffer.contents buf



(* CONTEXT *)

type context = {
    mutable symbols: symbol NotTrie.t;
    mutable constant_parsers: (string -> cexpr option) list;
    mutable frame: frame;
    mutable layers: layer list;
  }

let context_new () =
  {
    symbols = NotTrie.empty;
    constant_parsers = [];
    frame = frame_new ();
    layers = [];
  }

let context_add_constant_parser context parser =
  context.constant_parsers <- list_add_last context.constant_parsers parser

let context_add_variable context ?(for_coq = false) name noose =
  match context.layers with
  | [] ->
     let layer = layer_new () in
     context.layers <- [layer];
     layer_add_variable ~for_coq layer name noose
  | layer :: _ ->
     layer_add_variable ~for_coq layer name noose

let context_local_work context action =
  let context_push_layer context =
    context.layers <- (layer_new ()) :: context.layers
  and context_pop_layer context =
    match context.layers with
    | [] | [_] -> context.layers <- raise (Invalid_argument "won't happen!")
    | _ :: rest -> context.layers <- rest
  in
  context_push_layer context;
  try
    let res = action () in
    context_pop_layer context;
    res
  with whatever ->
        context_pop_layer context;
        raise whatever

let context_has_variable context name =
  exists (fun l -> layer_has_variable l name) context.layers

let context_variable_noose context name =
  let rec finder layers =
    match layers with
    | [] -> none
    | layer :: rest ->
       begin match layer_variable_noose layer name with
       | None -> finder rest
       | Some result -> some result
       end
  in
  finder context.layers

let context_variable_noose_tighten context name imposed =
  try
    let layer = find (fun l -> layer_has_variable l name) context.layers in
    let onoose = layer_variable_noose layer name in
    match onoose with
    | Some noose ->
       layer_add_variable layer name (noose_tighten context.frame noose imposed)
    | None ->
       () (* guaranteed by the find above *)
  with
    Not_found -> raise (Invalid_argument ("this context doesn't have a variable named `" ^ name ^ "`"))

let context_add_coercion context name source target =
  frame_add_coercion context.frame name source target;
  context.symbols <- NotTrie.map (fun symbol -> symbol_normalize symbol context.frame) context.symbols

let context_add_symbol context name level cexpr =
  context.symbols <- NotTrie.update name
                       (fun osymb
                        -> let symb = value ~default:(symbol_new name) osymb in
                           some (symbol_add_instance symb level cexpr))
                             context.symbols

let context_parse_symbol context name =
  match NotTrie.find_opt name context.symbols with
  | Some symbol ->
     symbol
  | None ->
     begin
       match filter_map (fun parser -> parser name) context.constant_parsers with
       | [] ->
          raise (Invalid_argument ("Constant symbol `" ^ name ^ "` is unknown in the context!"))
       | cexprs ->
          let raw =
            fold_left
              (fun symb cexpr -> symbol_add_instance symb (ctype_of_cexpr cexpr) cexpr)
              (symbol_new name)
              cexprs
          in
          symbol_normalize raw context.frame
     end

let string_of_context ?(for_coq = false) context =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let string_of_layers () =
    add_string "\n(* layers of fuzzy variables *)\n\n";
    iter (fun layer -> add_string (string_of_layer ~for_coq layer)) context.layers;
    add_string "\n\n"
  and string_of_symbols () =
    add_string "\n(* available symbols for elaboration:\n";
    NotTrie.iter (fun _ symbol ->
        add_string "\t";
        add_string (string_of_symbol symbol);
        add_string ": ";
        add_string (string_of_noose (symbol_noose context.frame symbol));
        add_string "\n") context.symbols;
    add_string "*)\n"
  in
  string_of_symbols ();
  string_of_layers ();
  Buffer.contents buf
