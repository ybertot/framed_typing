(* in this file:

- prepare a frame, add hypotheses to context, get saturating ones added automatically

- with proofs!

defects :

- how to name a pushed hypothesis?

- there are places where proofs don't get really handled

 *)

(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     fold (option_list_of_list_option tail)
       ~none: none
       ~some:(fun tail_ -> some (head :: tail_))
  | None :: tail -> none
  | _ -> some []

let rec chain_of_responsibility workers work =
  match workers with
  | worker :: tail -> fold (worker work)
                        ~none:(chain_of_responsibility tail work)
                        ~some:(fun result -> some result)
  | _ -> none

(* COQ TYPES *)
(* simplified... *)

type ctype = | CBasic of string
             | CFunctional of ctype * (ctype list)

let ctype_comp ct1 ct2 =
  match ct1 with
  | CBasic _ -> none
  | CFunctional (result, []) -> none
  | CFunctional (result, [head] ) ->
     if head = ct2 then some result
     else none
  | CFunctional (result, head :: tail ) ->
     if head = ct2 then some (CFunctional (result, tail))
     else none

let rec ctype_comps ct1 cts =
  match cts with
  | [] -> some ct1
  | ct :: other ->
     let* ctnext = ctype_comp ct1 ct in
     ctype_comps ctnext other

let rec string_of_ctype ct =
  match ct with
  | CBasic name -> name
  | CFunctional (result, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     iter (fun arg -> add_string (string_of_ctype arg);
                      add_string " -> ") args;
     add_string (string_of_ctype result);
     add_string ")";
     Buffer.contents buf

let ctype_predicate ct =
  CFunctional (CBasic "Prop", [ct])

let ctype_relation ct =
  CFunctional (CBasic "Prop", [ct; ct])

let ctype_operator ct =
  CFunctional (ct, [ct; ct])

(* COQ EXPRESSIONS *)
(* simplified... *)

type cexpr = | CConstant of string * ctype (* name and type *)
             | CApp of cexpr * cexpr list

let rec cexpr_typecheck ce =
  match ce with
  | CConstant (_, ct) -> some ct
  | CApp (fn, args) ->
     let* fnct = cexpr_typecheck fn in
     let* argsct = option_list_of_list_option (map cexpr_typecheck args) in
     ctype_comps fnct argsct

let rec string_of_cexpr ?(with_ctype = true) ce =
  match ce with
  | CConstant (name, _) ->
     if with_ctype then (
       let buf = Buffer.create 0 in
       let add_string = Buffer.add_string buf in
       add_string name;
       add_string ": ";
       (match cexpr_typecheck ce with
        | Some ct -> add_string (string_of_ctype ct)
        | None -> add_string "impossible!");
       Buffer.contents buf
     ) else name
  | CApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_cexpr ~with_ctype:false fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_cexpr ~with_ctype:false arg)) args;
     add_string ")";
     if with_ctype then (
       add_string ": ";
       match cexpr_typecheck ce with
       | Some ct -> add_string (string_of_ctype ct)
       | None -> add_string "impossible!"
     );
     Buffer.contents buf

(* CONTEXT *)

type hypothesis = string * cexpr (* name expression *)

type lemma = string * cexpr * string (* name expression proof *)

type arrow = hypothesis -> (cexpr * string) option (* from a proposition and its proof, get if possible the pushed proposition and its proof *)

type context = {
    mutable hypotheses : hypothesis list;
    mutable lemmas : lemma list;
    mutable arrows : arrow list (* this part is the actual frame *)
  }

let context_empty () = { hypotheses = []; lemmas = []; arrows = [] }

let context_add_lemma context name cexpr proof =
  context.lemmas <- (name, cexpr, proof) :: context.lemmas

let context_add_hypothesis context basename cexpr =
  let helper arrow =
    match arrow (basename, cexpr) with
    | Some (h, proof) ->
       context_add_lemma context (basename ^ "Z") h proof (* FIXME: name the pushed hypothesis? *)
    | _ -> ()
  in
  context.hypotheses <- (basename, cexpr) :: context.hypotheses;
  context.lemmas <- (basename, cexpr, basename) :: context.lemmas;
  iter helper context.arrows

let context_add_arrow context arrow =
  context.arrows <- arrow :: context.arrows

let print_context context buf =
  let add_string = Buffer.add_string buf in
  let hyp_helper (name, cexpr) =
    add_string "Hypothesis ";
    add_string name;
    add_string ": ";
    add_string (string_of_cexpr cexpr);
    add_string ".\n"
  and lemma_helper (name, expression, proof) =
    if name != proof then ( (* an hypothesis is its own proof *)
      add_string "Lemma ";
      add_string name;
      add_string ": ";
      add_string (string_of_cexpr expression);
      add_string ".\n";
      add_string "Proof.\n";
      add_string "  exact (";
      add_string proof;
      add_string ").\n";
      add_string "Qed.\n"
    )
  in
  iter hyp_helper context.hypotheses;
  iter lemma_helper context.lemmas

let arrow_relation srcname srctype dstname dsttype arrowname arrow = (* FIXME: awful *)
  fun (proof, cexpr) ->
  match cexpr with
  | CApp (CConstant(name_, CFunctional (CBasic "Prop", [type1_; type2_])), [arg1_; arg2_]) ->
     if name_ = srcname && type1_ = type2_ && type1_ = srctype then
       let* arg1, _ = arrow ("FIXME", arg1_) in
       let* arg2, _ = arrow ("FIXME", arg2_) in
       let buf = Buffer.create 0 in
       let add_string = Buffer.add_string buf in
       add_string arrowname;
       add_string " (";
       add_string (string_of_cexpr ~with_ctype:false arg1_);
       add_string ") (";
       add_string (string_of_cexpr ~with_ctype:false arg2_);
       add_string ") ";
       add_string proof;
       some (CApp (CConstant (dstname, ctype_relation dsttype),
                   [arg1; arg2]), Buffer.contents buf)
     else none
  | _ -> none

(* N *)

let c_le = CConstant ("le", ctype_relation (CBasic "nat"))

(* Z *)

let  c_z_of_n arg =
  CApp (CConstant ("Z.of_nat", CFunctional (CBasic "Z", [CBasic "nat"])), [arg])

let c_zle = CConstant ("Z.le", ctype_relation (CBasic "Z"))

(* N TO Z *)

let rec z_of_n_arrow (proof, cexpr) =
  let constant_arrow (prf, ce) =
    match ce with
    | CConstant (name, CBasic "nat") ->
       let result = c_z_of_n cexpr in
       some (result, string_of_cexpr result)
    | _ -> none
  and eq_arrow =
    arrow_relation "eq" (CBasic "nat") "eq" (CBasic "Z") "inj_eq" z_of_n_arrow
  and le_arrow =
    arrow_relation "le" (CBasic "nat") "Z.le" (CBasic "Z") "inj_le" z_of_n_arrow
  in
  chain_of_responsibility [constant_arrow; eq_arrow; le_arrow] (proof, cexpr)

(* TEST CODE *)

let print_opening buf =
  let add_string = Buffer.add_string buf in
  let add_line line = add_string line; add_string "\n" in
  add_line "Require Import ZArith.";
  add_line "Section Main."

let print_closing buf = Buffer.add_string buf "End Main.\n"

let main () =
  let buf = Buffer.create 0 in
  let context = context_empty () in
  let h = CApp (c_le, [CConstant ("2", (CBasic "nat")); CConstant ("3", (CBasic "nat"))]) in
  context_add_arrow context z_of_n_arrow;
  context_add_hypothesis context "H" h;
  print_opening buf;
  print_context context buf;
  print_closing buf;
  print_string (Buffer.contents buf)

;; main ()
