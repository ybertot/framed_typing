(* in this file:

- untyped expressions get interpreted as typed expression using a
   single-pass down-to-top with all possible interpretations

defects:

- single pass means it works only on very simple closed formulas

- something as simple as "eq 1 1" never gets typed correctly

- no coercion!

 *)



(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     let* tail_ = option_list_of_list_option tail in
     some (head :: tail_)
  | None :: tail -> none
  | [] -> some []

let rec list_product_map (f: 'a -> 'b list) (l: 'a list) =
  match l with
  | [] -> []
  | head :: [] -> map (fun a -> [a]) (f head)
  | head :: tail -> concat_map
                      (fun l -> map (fun a -> a :: l) (f head))
                      (list_product_map f tail)



(* UNTYPED EXPRESSIONS *)

type uexpr = | UConst of string
             | UApp of uexpr * uexpr list

let rec string_of_uexpr ue =
  match ue with
  | UConst name -> name
  | UApp (fn, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string "(";
     add_string (string_of_uexpr fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_uexpr arg)) args;
     Buffer.add_string buf ")";
     Buffer.contents buf



(* COQ TYPES *)
(* simplified... *)

type ctype = | CBasic of string
             | CFunct of ctype * ctype list

let ctype_predicate ct = CFunct (CBasic "Prop", [ct])
let ctype_relation ct = CFunct (CBasic "Prop", [ct; ct])
let ctype_operator ct = CFunct (ct, [ct; ct])

let string_of_ctype ct =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let rec helper ct_ =
    match ct_ with
    | CBasic name -> add_string name
    | CFunct (result, args) ->
       add_string "(";
       iter (fun arg -> helper arg;
                        add_string " -> ") args;
       helper result;
       add_string ")"
  in
  helper ct;
  Buffer.contents buf

let ctype_comp ct1 ct2 =
  match ct1 with
  | CFunct (result, [arg] ) ->
     if arg = ct2
     then some result
     else none
  | CFunct (result, arg :: others ) ->
     if arg = ct2
     then some (CFunct (result, others))
     else none
  | _ -> none

let rec ctype_comps ct1 cts =
  match cts with
  | ct :: others ->
     let* next = ctype_comp ct1 ct in
     ctype_comps next others
  | [] -> some ct1



(* COQ EXPRESSIONS *)
(* somewhat simplified of course, but that's the goal *)

type cexpr = | CConst of string * ctype
             | CApp of cexpr * cexpr list

(* accept mal-formed expression, because we use that to detect possible elaborations! *)
let rec ctype_of_cexpr ce =
  match ce with
  | CConst (_, ct) -> some ct
  | CApp (fn_, args_) ->
     let* fn = ctype_of_cexpr fn_ in
     let* args = option_list_of_list_option (map ctype_of_cexpr args_) in
     ctype_comps fn args

let rec string_of_cexpr ?(with_ctype = true) ce =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let add_ctype ce_ =
    if with_ctype then
      begin
        match ctype_of_cexpr ce_ with
        | Some ctype ->
           add_string ": ";
           add_string (string_of_ctype ctype)
        | None ->
           add_string ": Type (* impossible! *)";
      end
  in
  begin match ce with
  | CConst (name, _) ->
     add_string name;
     add_ctype ce
  | CApp (fn, args) ->
     add_string "(";
     add_string (string_of_cexpr ~with_ctype:false fn);
     iter (fun arg -> add_string " ";
                      add_string (string_of_cexpr ~with_ctype:false arg)) args;
     add_string ")";
     add_ctype ce
  end;
  Buffer.contents buf



(* CONTEXT *)

module NotTrie = Map.Make(String) (* FIXME: I'd prefer a real trie *)

type context = {
    (* excarnated objects *)
    mutable symbols: cexpr list NotTrie.t;
    (* recognize what a constant can be *)
    mutable constant_parsers: (string -> cexpr option) list;
    (* variables in the Coq context *)
    mutable variables: ctype NotTrie.t;
  }

let context_empty () =
  {
    symbols = NotTrie.empty;
    constant_parsers = [];
    variables = NotTrie.empty;
  }

let context_add_symbol context name cexpr =
  context.symbols <- NotTrie.update name
                       (fun ores -> match ores with
                                    | Some res -> some (cexpr :: res)
                                    | None -> some [cexpr])
                       context.symbols

let context_add_variable context name ctype =
  context.variables <- NotTrie.update name (fun _ -> some ctype) context.variables;
  context_add_symbol context name (CConst (name, ctype))

let context_add_constant_parser context parser =
  context.constant_parsers <- parser :: context.constant_parsers

let context_parse_string context chaine =
  let symbol_parser name =
    match NotTrie.find_opt name context.symbols with
    | Some cexprs -> cexprs
    | None -> []
  in
  append (symbol_parser chaine) (filter_map (fun parser -> parser chaine) context.constant_parsers)

let string_of_context context =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let string_of_variables () =
    add_string "\n(* Coq variables *)\n\n";
    NotTrie.iter (fun key value ->
        add_string "Variable ";
        add_string key;
        add_string ": ";
        add_string (string_of_ctype value);
        add_string ".\n") context.variables;
    add_string "\n\n"
  and string_of_symbols () =
    add_string "\n(* available symbols for elaboration:\n";
    NotTrie.iter
      (fun key cexprs ->
        add_string "\t";
        add_string key;
        add_string ":\n";
        ignore begin
            map (fun ce ->
                add_string "\t\t";
                add_string (string_of_cexpr ce);
                add_string "\n") cexprs
          end)
      context.symbols;
    add_string "*)\n"
  in
  string_of_symbols ();
  string_of_variables ();
  Buffer.contents buf

let rec context_elaborate context uexpr =
  match uexpr with
  | UConst name -> context_parse_string context name
  | UApp (fn, args) ->
     let fns = context_elaborate context fn in
     let args = list_product_map (context_elaborate context) args in
     let possible = concat_map (fun fn_ -> map (fun args_ -> CApp (fn_, args_)) args) fns in
     filter (fun ce -> is_some (ctype_of_cexpr ce)) possible



(* LEARN ABOUT COQ STUFF *)
(* this is needed because we're out of Coq *)

let add_coq_to_context context =
  let add name ctype =
    context_add_symbol context name (CConst (name, ctype))
  in
  begin
    let typename = CBasic "Prop" in
    add "True" typename;
    add "False" typename;
    add "and" (ctype_relation typename);
    add "or" (ctype_relation typename);
    add "not" (ctype_predicate typename)
  end;
  begin
    let typename = CBasic "N" in
    add "N.eq" (ctype_relation typename);
    add "N.le" (ctype_relation typename);
    add "N.add" (ctype_operator typename);
    add "N.mul" (ctype_operator typename)
  end;
  begin
    let typename = CBasic "Z" in
    add "Z.eq" (ctype_relation typename);
    add "Z.le" (ctype_relation typename);
    add "Z.add" (ctype_operator typename);
    add "Z.mul" (ctype_operator typename)
  end



(* N *)

let add_N_to_context context =
  let ctype = CBasic "N"
  and add = context_add_symbol context
  in
  let cst_parser value =
    if Str.string_match (Str.regexp "[0-9]+$") value 0
    then some (CConst (value ^ "%N", ctype))
    else none
  in
  context_add_constant_parser context cst_parser;
  add "eq" (CConst ("N.eq", ctype_relation ctype));
  add "add" (CConst ("N.add", ctype_operator ctype));
  add "mul" (CConst ("N.mul", ctype_operator ctype))



(* Z *)

let add_Z_to_context context =
  let ctype = CBasic "Z"
  and add = context_add_symbol context
  in
  let cst_parser value =
    if Str.string_match (Str.regexp "-?[0-9]+$") value 0 then
      begin
        if String.starts_with ~prefix:"-" value
        then some (CConst ("(" ^ value ^ ")%Z", ctype))
        else some (CConst (value ^ "%Z", ctype))
      end
    else none
  in
  context_add_constant_parser context cst_parser;
  add "eq" (CConst ("Z.eq", ctype_relation ctype));
  add "add" (CConst ("Z.add", ctype_operator ctype));
  add "sub" (CConst ("Z.sub", ctype_operator ctype));
  add "mul" (CConst ("Z.mul", ctype_operator ctype))



(* TEST CODE *)

let print_opening buf =
  let add_string = Buffer.add_string buf in
  let add_line line = add_string line; add_string "\n" in
  add_line "Require Import ZArith.";
  add_line "Section Main."

let print_closing buf = Buffer.add_string buf "End Main.\n"

let main () =
  let buf = Buffer.create 0 in
  let add_string = Buffer.add_string buf in
  let context = context_empty () in
  let helper ue =
    add_string "(* trying to incarnate ";
    add_string (string_of_uexpr ue);
    add_string " *)\n";
    begin match context_elaborate context ue with
    | [] ->
       add_string "(* nothing - might need coercions! *)\n"
    | [ce] ->
       add_string "Check ";
       add_string (string_of_cexpr ce);
       add_string ".\n"
    | ces ->
       add_string "(* found several:\n";
       ignore begin
           map (fun ce ->
               add_string "\t";
               add_string (string_of_cexpr ce);
               add_string "\n")
             (context_elaborate context ue)
         end;
       add_string "*)\n"
    end
  in
  add_coq_to_context context;
  add_N_to_context context;
  add_Z_to_context context;
  context_add_variable context "a" (CBasic "N");
  context_add_variable context "b" (CBasic "N");
  context_add_variable context "c" (CBasic "Z");
  print_opening buf;
  add_string (string_of_context context);
  ignore begin
      map helper [
          UConst "a";
          UConst "N.add";
          UApp (UConst "N.add", [UConst "a"]);
          UApp (UConst "N.add", [UConst "a"; UConst "2"]);
          UApp (UConst "Z.add", [UConst "c"; UConst "2"]);
          UApp (UConst "Z.add", [UConst "c"; UConst "-2"]);
          UApp (UConst "not", [UConst "True"]);
          UApp (UConst "sub", [UConst "c"; UConst "c"]);
          UApp (UConst "eq", [UConst "a"; UConst "b"]);
          UApp (UConst "eq", [UConst "1"; UConst "-1"]);
          UApp (UConst "eq", [UConst "1"; UConst "1"]);
          UApp (UConst "eq", [UConst "a"; UConst "c"]);
        ]
    end;
  print_closing buf;
  print_string (Buffer.contents buf)

;;

main ()
