(* in this file:

- take a typed expression and try to saturate it in a frame

defects:

- as the "set" is used to know if something was pushed somewhere, that
   means I had to make predicates and relations go to a typed
   proposition type!

- the notion of frame is too simple, so defining one is a bit too
   involved

- no proof!

 *)

(* HELPING TOOLS *)

open Option
open List

let (let*) = bind

let rec option_list_of_list_option listops =
  match listops with
  | (Some head) :: tail ->
     (match option_list_of_list_option tail with
      | Some tail_ -> some (head :: tail_)
      | _ -> none)
  | None :: tail -> none
  | _ -> some []

let rec chain_of_responsibility workers work =
  match workers with
  | worker :: tail ->
     (match worker work with
      | Some result -> some result
      | _ -> chain_of_responsibility tail work)
  | _ -> none

(* TYPES *)

type ttype = | TBasic of string
             | TProp of ttype (* BAD: I need to distinguish an equality in N, Z, etc *)
             | TMap of ttype * (ttype list) (* result type, argument types *)

let rec string_of_ttype ttype =
  match ttype with
  | TBasic value -> value
  | TProp value -> "prop (" ^ (string_of_ttype value) ^ ")"
  | TMap (result, args) ->
     if args = []
     then string_of_ttype result
     else (let buf = Buffer.create 0 in
           let add_string = Buffer.add_string buf in
           iter (fun arg -> add_string (string_of_ttype arg) ; add_string " -> ") args;
           add_string (string_of_ttype result);
           Buffer.contents buf)

(* EXPRESSIONS *)

type texpr = | TConstant of ttype * string
             | TVariable of ttype * string
             | TFunction of string * ttype * texpr list

let texpr_ttype: texpr -> ttype = (* determine the overall type of a typed expression *)
  fun te -> match te with
            | TConstant (tt, _) -> tt
            | TVariable (tt, _) -> tt
            | TFunction (_, tt, _) -> tt

let rec string_of_texpr texpr =
  match texpr with
  | TConstant (ttype, value) -> value ^ ": " ^ (string_of_ttype ttype)
  | TVariable (ttype, name) -> name ^ ": " ^ (string_of_ttype ttype)
  | TFunction (name, ttype, args) ->
     let buf = Buffer.create 0 in
     let add_string = Buffer.add_string buf in
     add_string name;
     iter (fun arg -> add_string " (";
                           add_string (string_of_texpr arg);
                           add_string ")") args;
     add_string ": ";
     add_string (string_of_ttype ttype);
     Buffer.contents buf

(* FRAME *)

type caster = ttype * ttype * (texpr -> texpr option) (* source, destination and arrow *)
(* notice: the result is an option because N -> Z always works but Z -> N might fail *)

type frame = caster list (* extremely basic *)

(* computing the saturation is a bit involved, as:

 - we need to keep track of where we put things to avoid pushing them
   there again

 - we loop over the casters in the frame as long as we managed to push
   anything anywhere (using the go_on flag)

 *)
let texpr_saturate texpr frame =
  let caster_helper (assoc, go_on) (srcttype, dstttype, arrow) =
    match assoc_opt srcttype assoc, assoc_opt dstttype assoc with
    | Some srctexpr, None -> (* check we have something for the source and not the target *)
       (match arrow srctexpr with
        | Some dsttexpr -> (dstttype, dsttexpr)::assoc, true (* we managed to push! *)
        | _ -> assoc, go_on)
    | _ -> (assoc, go_on)
  in
  let rec saturate_helper initassoc =
    match fold_left caster_helper (initassoc, false) frame with
    | (next, true) -> saturate_helper next
    | (next, _) -> next
  in
  map snd (saturate_helper [(texpr_ttype texpr, texpr)])

(* the next functions are there to help implement specific casters *)

let tfunction_drop srcttype dstttype name =
  fun texpr ->
  match texpr with
  | TFunction (name_, dstttype_, [arg]) ->
     if name_ = name && dstttype_ = dstttype
     then some arg
     else none
  | _ -> none

let tfunction_predicate_caster_from_assoc caster srcttype dstttype pairs =
  fun texpr ->
  match texpr with
  | TFunction (srcname, TProp srcttype_, srcargs) ->
     if srcttype_ = srcttype then
       let* dstname = assoc_opt srcname pairs in
       let* dstargs = option_list_of_list_option (map caster srcargs) in
       some (TFunction (dstname, TProp dstttype, dstargs))
     else none
  | _ -> none

let tfunction_operator_caster_from_assoc caster srcttype dstttype pairs =
  fun texpr ->
  match texpr with
  |TFunction (srcname, srcttype_, srcargs) ->
    if srcttype_ = srcttype then
      let* dstname = assoc_opt srcname pairs in
      let* dstargs = option_list_of_list_option (map caster srcargs) in
      some (TFunction (dstname, dstttype, dstargs))
    else none
  | _ -> none

(* N -> Z CASTER CODE *)

let tn = TBasic "N" (* two-letter identifiers are bad, I know... *)
let tz = TBasic "Z" (* two-letter identifiers are bad, I know... *)

let z_of_n arg = TFunction ("Z_of_N", tz, [arg])

let rec z_of_n_arrow texpr =
  let constants caster =
    fun te ->
    match te with
    | TConstant (tn, value) -> some (TConstant (tz, value))
    | _ -> none
  and operators caster =
    tfunction_operator_caster_from_assoc caster tn tz [("add.N", "add.Z");
                                                       ("mul.N", "mul.Z")]
  in
  let workers = map (fun preworker -> preworker z_of_n_arrow) [constants;
                                                                    operators]
  in
  match chain_of_responsibility workers texpr with
  | Some result -> some result
  | _ ->
     if texpr_ttype texpr = tn
     then some (z_of_n texpr)
     else none

let z_of_n_expr_caster = (tn, tz, z_of_n_arrow)

let z_of_n_predicate_caster =
  let predicates =
    tfunction_predicate_caster_from_assoc z_of_n_arrow tn tz
      [("nonzero.N", "nonzero.Z");
       ("eq.N", "eq.Z");
       ("le.N", "le.Z");
       ("lt.N", "lt.Z")]
  in
  (TProp tn, TProp tz,  predicates)

let z_of_n_casters = [z_of_n_expr_caster; z_of_n_predicate_caster]

(* Z -> N CODE *)

let rec n_of_z_arrow texpr =
  let constants caster =
    fun te ->
    match te with
    | TConstant (tz, value) ->
       let nat = Str.regexp "[0-9]+" in
       if Str.string_match nat value 0
       then some (TConstant (tn, value))
       else none
    | _ -> none
  and operators caster =
    tfunction_operator_caster_from_assoc caster tz tn [("add.Z", "add.N");
                                                       ("mul.Z", "mul.N")]
  and dropper caster = tfunction_drop tn tz "Z_of_N"
  in
  let workers = map (fun preworker -> preworker n_of_z_arrow) [constants;
                                                                    operators;
                                                                    dropper]
  in
  chain_of_responsibility workers texpr

let n_of_z_expr_caster = (tz, tn, n_of_z_arrow)

let n_of_z_predicate_caster =
  let predicates =
    tfunction_predicate_caster_from_assoc n_of_z_arrow tz tn
      [("nonzero.Z", "nonzero.N");
       ("eq.Z", "eq.N");
       ("le.Z", "le.N");
       ("lt.Z", "lt.N")]
  in
  (TProp tz, TProp tn, predicates)

let n_of_z_casters = [n_of_z_expr_caster; n_of_z_predicate_caster]

(* Z TO C CODE *)

let tc = TBasic "C" (* two-letter identifiers are bad, I know... *)

let c_of_z arg = TFunction ("C_of_Z", tc, [arg])

let rec c_of_z_arrow texpr =
  let constants caster =
    fun te ->
    match te with
    | TConstant (tz, value) -> some (TConstant (tc, value))
    | _ -> none
  and operators caster =
    tfunction_operator_caster_from_assoc caster tz tc [("add.Z", "add.C");
                                                       ("mul.Z", "mul.C")]
  in
  let workers = map (fun preworker -> preworker c_of_z_arrow) [constants;
                                                                    operators]
  in
  match chain_of_responsibility workers texpr with
  | Some result -> some result
  | None -> some (c_of_z texpr)

let c_of_z_expr_caster = (tz, tc, c_of_z_arrow)

let c_of_z_predicate_caster =
  let predicates =
    tfunction_predicate_caster_from_assoc c_of_z_arrow tz tc
      [("nonzero.Z", "nonzero.C");
       ("eq.Z", "eq.C")]
  in
  (TProp tz, TProp tc, predicates)

let c_of_z_casters = [c_of_z_expr_caster; c_of_z_predicate_caster]

(* DEFAULT FRAME *)

let default_frame = flatten [z_of_n_casters;
                             n_of_z_casters;
                             c_of_z_casters]

(* TEST CODE *)

let test_saturate () =
  let test_saturate_helper texpr =
    print_string ("\tInitial expression: " ^ (string_of_texpr texpr) ^ "\n");
    iter (fun variant -> print_string ("\t\t" ^ (string_of_texpr variant) ^ "\n"))
      (texpr_saturate texpr default_frame)
  in
  print_string "Saturation tests:\n";
  iter test_saturate_helper
    [TConstant (tn, "2");
     TConstant (tz, "2");
     TConstant (tz, "-2");
     TVariable (tn, "a");
     TFunction ("Z_of_N", tz, [TVariable (tn, "a")]);
     TFunction ("add.N", tn, [TConstant (tn, "2"); TVariable (tn, "a")]);
     TFunction ("add.Z", tz, [TConstant (tz, "2"); z_of_n (TVariable (tn, "a"))]);
     TFunction ("mul.Z", tz, [TConstant (tz, "-3"); TVariable (tz, "a")]);
     TFunction ("nonzero.N", TProp tn, [TConstant (tn, "3")]);
     TFunction ("le.Z", TProp (tz), [TConstant (tz, "3"); z_of_n (TVariable (tn, "a"))])
    ]
;;

test_saturate ()
